<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
  |--------------------------------------------------------------------------
  | User Defined Constants
  |--------------------------------------------------------------------------
 */

$root = "http://" . $_SERVER['HTTP_HOST'] . str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']) . 'uploads';

/* * ******** Generic Starts ********** */
// Site Constants
define('SITETHEME', 'theme/');
define('DASHTHEME', 'theme/frontend/idash/');
define('USERTHEME', 'theme/frontend/iuser/');
define('ADMINTHEME', 'theme/admin/');
define('COMMONTHEME', 'theme/common/');
define('UPLOADIMAGESPATH', 'theme/images/upload/');
define('GALLERYIMAGESPATH', 'theme/images/upload/gallery/');
define('UPLOADPROFILEPICPATH', 'theme/images/upload/profile/');
define('SITETITLE', 'QAZI AGRI FARMS');
define('LINK', 'https://www.qaziagrifarms.com');
define('SITELINK', 'https://www.qaziagrifarms.com');
define('SUPPORT_EMAIL', 'info@qaziagrifarms.com');
define('INFO_EMAIL', 'info@qaziagrifarms.com');
define('CONTACT_US_EMAIL', 'contact@qaziagrifarms.com');
define('SUPPORT_CONTACT', '+92 321 6156214');
define('SUPPORT_CONTACT_LINK', '13069121330');
define('SUPPORT_ADDRESS', 'Daska, Sialkot, Pakistan');
define('DIR_SEP', '/');
define('GOOGLE_MAPS_API_EMBED', 'AIzaSyAMffOKmqCGRpOQt8BfTobFhhhuYVwbpbU');
define('GOOGLE_MAPS_API_LAT_LONG', 'AIzaSyAMffOKmqCGRpOQt8BfTobFhhhuYVwbpbU');
define('SENDGRID_API_KEY', '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');
define('SALT', 'DFsdfsdfDgGsGDFAIzaSyB1mSDDlhEsD7BHWXSphcGLERTpCEP8vqDFGD8MlrIPSSDDSFFGDF456sdf4');

define('REVIEW_PER_PAGE', '5');
define('RADIUS', '50');
define('CITY_RADIUS', '500');
define('SEARCH_LIMIT', '10');
define('OFFSET_LIMIT', '10');

// Seo Constants
define('SEO_TITLE', 'Welcome to qaziagrifarms.com');
define('SEO_KEYWORD', 'book event');
define('SEO_DESCRIPTION', 'Find the best venue for your Event!');
define('SEO_IMAGE', 'book-your-event.png');

// Image Path
define('CDNURL', $root . DIR_SEP);
define('UPLOAD_DIR', 'uploads' . DIR_SEP);
define('DEFAULT_DIR', 'defaults' . DIR_SEP);
define('SHARE_DIR', 'share' . DIR_SEP);
define('PROFILE_IMAGE_DIR', 'profile-images' . DIR_SEP);
define('ENTITY_IMAGE_DIR', 'entity-images' . DIR_SEP);
define('ORIGINAL_DIR', 'original' . DIR_SEP);
define('LARGE_DIR', 'large' . DIR_SEP);
define('MEDIUM_DIR', 'medium' . DIR_SEP);
define('THUMB_DIR', 'thumb' . DIR_SEP);


// Image Sizes to resize and crop for User Profile
define('PROFILE_LARGE_X', '946');
define('PROFILE_LARGE_Y', '946');
define('PROFILE_MEDIUM_X', '300');
define('PROFILE_MEDIUM_Y', '300');
define('PROFILE_THUMB_X', '40');
define('PROFILE_THUMB_Y', '40');

define('MAX_CART', '1');

// Image Sizes to resize and crop for Entity
define('ENTITY_LARGE_X', '980');
define('ENTITY_LARGE_Y', '700');
define('ENTITY_MEDIUM_X', '380');
define('ENTITY_MEDIUM_Y', '200');
define('ENTITY_THUMB_X', '100');
define('ENTITY_THUMB_Y', '70');

// Default Image Names
define('DEFAULT_PROFILE_IMAGE', 'default-profile.png');
define('DEFAULT_ENTITY_IMAGE', 'default-entity.png');

// Social Links
define('TWITTER', 'https://twitter.com/venuetop');
define('FACEBOOK', 'https://www.facebook.com/venuetop');
define('YOUTUBE', 'https://youtu.be/J0SPyTidQoU');
define('LINKEDIN', 'https://linkedin.com/company/venuetop');
define('PINTEREST', 'https://www.pinterest.ca/venuetop');
define('DRIBBBLE', 'https://dribbble.com/mabuc');
define('GPLUS', 'https://plus.google.com/u/1/112733371813294714020');
define('INSTAGRAM', 'https://instagram.com/envato');
define('RSS', 'http://feedburner.com');
/* * ******** Generic Ends ********** */

/* * ******** Frontend Site Messages Starts ********** */
define('LOGIN_SUCC_MSG', 'Login Successful.');
define('LOGOUT_SUCCESS', 'Logout Successful.');
define('LOGIN_FAIL_MSG', 'Login failed! Try again');
define('REG_SUCC_MSG', 'Registration done! Browse multiple homes for your need.');
define('REG_FAIL_MSG', 'Registration failed!');
define('FORGOT_MSG', 'Password reset email sent to registered email');
define('FORGOT_FAIL', 'Wrong user details. Contact us for more details');
define('CHANGEPASS_FAILURE', 'Sorry! Password could not be changed!');
define('CHANGEPASS_SUCCESS', 'Password changed successfully!');
define('FEEDBACK_SUCCESS', 'Thank you for Feedback. We will get back to you on your Query/Comments/Problem.');
define('FEEDBACK_FAILURE', 'Oops! There is a problem while sending your feedback.');
define('UPDATEPROFILE_SUCCESS', 'Profile updated successfully!');
define('UPDATEPROFILE_FAILURE', 'Sorry! Could not update your profile!');
define('PASS_RESET_SUCC_MSG', 'We have sent reset link to your Email');
define('PASS_RESET_FAIL_MSG', 'Sorry! Could not send reset link');
define('SUSPEND_ACCOUNT', 'Your account is suspended');
define('WENT_WRONG', 'Something has went wrong, Please contact Support. Thank you.');
define('ACT_FAIL_MSG', 'Activation failed! Invalid Activation Link.');
define('ACT_SUCC_MSG', 'Activation Successful! Please Login');
define('SESSION_EXPIRED', 'Your Session is Expired, Please Re-login');
define('SUCC_MSG', 'Data is saved sucessfully!');
define('FAIL_MSG', 'Sorry! Could not save the data');
define('SUCC_MSG_INQ', 'Your inquiry has been sent.');
define('FAIL_MSG_INQ', 'Sorry! Could not send the inquiry');
define('NEWSLETTER_SUB_SUCCESS_MSG', 'Thank you for your subscription!');
define('NEWSLETTER_SUB_FAIL_MSG', 'Sorry! We could not you in subscription list at this moment');
define('NEWSLETTER_ALREADY_SUB_MSG', 'Looks like this email is already on our list');
define('NEWSLETTER_UNSUB_SUCCESS_MSG', 'Good bye! we will miss you.');
define('NEWSLETTER_UNSUB_FAIL_MSG', 'Login failed!');
define('DEMO_SUCC_MSG', 'Thanks for your interest in qaziagrifarms.com demo. We will get back to you shortly.');
define('DEMO_FAIL_MSG', 'Sorry! Could not schedule the demo, Please try again.');
define('EMAILSHARE_SUCC_MSG', 'Thanks you so much. We have sent the details.');
define('EMAILSHARE_FAIL_MSG', 'Sorry! There is something wrong, Please try again later.');
define('NODATA', '<h2><font color="red">There is no data available.</font></h2>');
define('NORECORD', '<h2><font color="red">No Record Available.</font></h2>');
/* * ******** Frontend Site Messages Ends ********** */

/* * ******** Admin Panel Messages Starts ********** */
// Partner
define('ADD_PARTNER_SUCCESS', 'Partner profile has been created');
define('ADD_PARTNER_FAILURE', 'Sorry! Could not create Partner profile!');
define('UPDATE_PARTNER_SUCCESS', 'Partner details has been updated');
define('UPDATE_PARTNER_FAILURE', 'Sorry! Could not update Partner details!');
// Entity
define('ADD_ENTITY_SUCCESS', 'Entity details has been created');
define('ADD_ENTITY_FAILURE', 'Sorry! Could not create Entity details!');
define('UPDATE_ENTITY_SUCCESS', 'Entity details has been updated');
define('UPDATE_ENTITY_FAILURE', 'Sorry! Could not update Entity details!');
define('BLOCK_ENTITY_SUCCESS', 'Event venue is blocked for this date');
define('BLOCK_ENTITY_FAILURE', 'Sorry! Could not block the hall');
/* * ******** Admin Panel Messages Starts ********** */

if (ENVIRONMENT == 'development') {
    define('SMTPHOST', 'smtp.gmail.com');
    define('SMTPUSERNAME', 'venuetop@gmail.com');
    define('SMTPPASSWORD', '8Mi6d]i%Gv%VNU');
    define('SMTPSECURE', 'ssl');
    define('SMTPPORT', '465');
    define('FROMEMAIL', 'do-not-reply@qaziagrifarms.com');
    define('STRIPE_SECRET', 'sk_test_O8gWZbqjawY9CAkuiqRz96F8');
    define('STRIPE_PUBLIC', 'pk_test_I8iTCocQxQp6yizFjHe9AHZa');
} else {
    define('FROMEMAIL', 'do-not-reply@qaziagrifarms.com');
    define('STRIPE_SECRET', 'sk_live_AKPs68DqZXKbkU9qK9SYAanR');
    define('STRIPE_PUBLIC', 'pk_live_iAN8Y16L4ORJu59JCloYFiGZ');
}