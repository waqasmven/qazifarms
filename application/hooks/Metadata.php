<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Metadata {

    var $ci = null;

    function __construct() {
        $this->ci = &get_instance();
        $this->ci->load->database();
    }

   /* function get_metadata() {
        // return;
        $ci = $this->ci;
        $req_directory = '';
        $req_data = array(
            'class' => 'default',
            'method' => 'default',
            'parameter' => 'default'
        );
        $rsegments = '';
        $seo_data = '';

        if (isset($ci->router)) {
            if (isset($ci->router->uri)) {
                if (!is_null($ci->router->uri->rsegments)) {
                    $rsegments = $ci->router->uri->rsegments;
                    $segments = $ci->router->uri->segments;
                    $req_directory = $ci->router->directory;
                    $req_data['class'] = $rsegments[1];//array_shift($rsegments);
                    $req_data['method'] = $rsegments[2];//array_shift($rsegments);
                    $req_data['parameter'] = end($segments);
                    
                }
            }
        }

        if ($req_directory == '') {
            switch (strtolower($req_data['class'])) {
                case 'entity':
                    $req_data['permalink'] = $req_data['parameter'];
                    if ($req_data['method'] == 'details') {
                        $seo_data = $this->get_entity_meta($req_data);
                        if (array_key_exists('entity_id', $seo_data)
                            && $seo_data['entity_id'] != '') {
                            $this->ci->session->set_flashdata('entity_id', $seo_data['entity_id']);
                        } else {
                            redirect('/404');
                        }
                    } else if ($req_data['method'] == 'city_list') {
                        $seo_data = $this->get_city_meta($req_data);
                        $this->ci->session->set_flashdata('parameter', $req_data['parameter']);
                    } else {
                        $seo_data = $this->get_page_meta($req_data);
                    }

                    break;
                case
                'search':
                    break;
                case 'pages': // For static pages
                    $seo_data = $this->get_page_meta($req_data);
                    break;
                default:
                    $seo_data = $this->get_page_meta($req_data);
            }
        } else {
            // request dir is either relanet or partners
            $seo_data = $this->get_page_meta($req_data);
        }
        // switch case based on class
        if ($seo_data == '') {
            $this->set_static_seo($seo_data);
        }
        MY_Loader::$add_data['meta'] = $seo_data;
    }

    private function set_static_seo(&$seo_data) {
        $seo_data = array(
            'title' => SEO_TITLE,
            'keyword' => SEO_KEYWORD,
            'description' => SEO_DESCRIPTION,
            'image' => base_url() . UPLOAD_DIR . DEFAULT_DIR . SHARE_DIR . SEO_IMAGE
        );
    }

    private function get_page_meta($meta = array()) {
        $get_default_meta_query = "SELECT `title`, `keyword`, `description`, CONCAT( '" . base_url() . UPLOAD_DIR . DEFAULT_DIR . SHARE_DIR . "',`image`) as image FROM `seo_master` WHERE `status` = 'ACTIVE' AND `class` = :class AND method = :method 
                                    UNION
                                   SELECT `title`, `keyword`, `description`, CONCAT( '" . base_url() . UPLOAD_DIR . DEFAULT_DIR . SHARE_DIR . "' ,`image`) AS image  FROM `seo_master` WHERE `status` = 'ACTIVE' AND `class` = 'default' AND method = 'default'";
        $get_default_meta_stmt = $this->ci->db->conn_id->prepare($get_default_meta_query);
        $get_default_meta_stmt->execute(array(
            ':class' => $meta['class'],
            ':method' => $meta['method']
        ));
        $data = $get_default_meta_stmt->fetch(PDO::FETCH_ASSOC);
        if (@count($data) > 0) {
            return $data;
        } else {
            return '';
        }
    }

    private function get_entity_meta($meta = array()) {

        $get_default_meta_query = "SELECT  *  FROM (SELECT em.entity_id, `title`,`keyword`,`description`, CONCAT( '" . base_url() . UPLOAD_DIR . ENTITY_IMAGE_DIR . "', em.entity_id, '" . DIR_SEP . "' ,`image`) AS image, CONCAT('Book ', entity_disp, ' in ', city_name) AS dynamic_keyword1, CONCAT('Book ',  GROUP_CONCAT(venue_type) , ' in ', city_name) 
                                    AS dynamic_keyword2 FROM `seo_master` sm   JOIN `entity_master_list`  em ON 
                                    sm.parameter = em.permalink  AND em.permalink = :permalink 
                                     JOIN `entity_property_type_event_type_list` eptetl ON em.entity_id = eptetl.entity_id
                                     WHERE sm.`status` = 'ACTIVE' AND sm.`class` = :class AND sm.method = :method
                                     UNION 
                                     SELECT '' AS entity_id, `title`,`keyword`, 
                                    `description`,CONCAT( '" . base_url() . UPLOAD_DIR . DEFAULT_DIR . SHARE_DIR . "' ,`image`) AS image, '' AS 
                                    dynamic_keyword1, '' AS dynamic_keyword2 FROM `seo_master` WHERE `status` = 'ACTIVE' AND 
                                    `class` = 'default' AND method = 'default') t1
WHERE
    entity_id IS NOT NULL";

        $get_default_meta_stmt = $this->ci->db->conn_id->prepare($get_default_meta_query);
        $get_default_meta_stmt->execute(array(
            ':permalink' => $meta['permalink'],
            ':class' => $meta['class'],
            ':method' => $meta['method']
        ));
        $data = $get_default_meta_stmt->fetch(PDO::FETCH_ASSOC);

        if (count($data) > 0 && $data['entity_id'] > 0) {
            $seo_data = array(
                'entity_id' => $data['entity_id'] != '' ? $data['entity_id'] : '0',
                'title' => $data['title'],
                'keyword' => $data['keyword'] . ($data['dynamic_keyword1'] != '' ? ', ' . $data['dynamic_keyword1'] : '') .
                    ($data['dynamic_keyword2'] != '' ? ', ' . $data['dynamic_keyword2'] : ''),
                'description' => $data['description'],
                'image' => $data['image']
            );
            return $seo_data;
        } else {
            return $data;
        }
    }

    private function get_city_meta($meta = array()) {

        $get_default_meta_query = " SELECT '' AS entity_id, sm.`title`, sm.`keyword`, 
                                    sm.`description`,CONCAT( '" . base_url() . UPLOAD_DIR . DEFAULT_DIR . SHARE_DIR . "' ,sm.`image`) 
                                    AS image, '' AS 
                                    dynamic_keyword1, '' AS dynamic_keyword2 FROM `seo_master` sm
                                    WHERE `status` = 'ACTIVE' AND 
                                    `class` = :class AND method = :method AND parameter = :permalink
                                    UNION 
                                     SELECT '' AS entity_id, `title`,`keyword`, 
                                    `description`,CONCAT( '" . base_url() . UPLOAD_DIR . DEFAULT_DIR . SHARE_DIR . "' ,`image`) AS image, '' AS 
                                    dynamic_keyword1, '' AS dynamic_keyword2 FROM `seo_master` WHERE `status` = 'ACTIVE' AND 
                                    `class` = 'default' AND method = 'default' ";

        $get_default_meta_stmt = $this->ci->db->conn_id->prepare($get_default_meta_query);
        $get_default_meta_stmt->execute(array(
            ':permalink' => $meta['permalink'],
            ':class' => $meta['class'],
            ':method' => $meta['method']
        ));
        $data = $get_default_meta_stmt->fetch(PDO::FETCH_ASSOC);
        return $data;
    }
*/
}
