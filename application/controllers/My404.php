<?php

class My404 extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->output->set_status_header('404');
        $data['heading'] = '404 Page';
        $this->load->view('admin/404',$data); //loading in my template
    }

}

?>