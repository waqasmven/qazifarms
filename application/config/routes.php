<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'pages';
$route['commercial'] = 'home/commercial';
$route['property-list/(:any)'] = 'search/property_list';
$route['search'] = 'search/index';
$route['properties/([^/]+)'] = 'entity/property_details/1$';
$route['compare'] = 'entity/compare';
$route['user'] = 'user/1$';
$route['services/([^/]+)'] = 'entity/services/1$';
$route['service/([^/]+)'] = 'entity/service_details/1$';

// user routes
$route['login'] = 'admin/Admin_con';
$route['register'] = 'user/register';


// Pages
$route['About-us'] = 'pages/about_Us';
$route['terms'] = 'pages/terms';
$route['Contact-us'] = 'pages/contact_us';
$route['Gallery'] = 'pages/gallery';
$route['Services'] = 'pages/services';

// Dashboard
$route['dashboard'] = 'admin/Admin_con/dashboard';
$route['dashboard/update/main-page-text'] = 'admin/Admin_con/update_main_page_text_data';
$route['dashboard/view/slide-show'] = 'admin/Admin_con/get_all_slide_show_data';
$route['dashboard/view/slide-show-pic/(:any)'] = 'admin/Admin_con/get_slide_show_pic_data/$1';
$route['dashboard/view/gallery-pic/(:any)'] = 'admin/Admin_con/get_gallery_pic_data/$1';
$route['dashboard/delete/slide-show-pic/(:any)'] = 'admin/Admin_con/delete_slide_show_pic/$1';
$route['dashboard/updated/slide_show_pic'] = 'admin/Admin_con/slide_show_pic/';
$route['dashboard/add/new/slide-show-pic'] = 'admin/Admin_con/insert_new_slide_show_pic/';
$route['dashboard/add/new/gallery'] = 'admin/Drop_zone/add_gallery/';
$route['dashboard/view/gallery'] = 'admin/Admin_con/get_all_gallery_data/';
//users
$route['dashboard/users-list'] = 'admin/Admin_con/get_all_users/';
$route['dashboard/disabled-user-list'] = 'admin/Admin_con/get_disabled_users/';
$route['dashboard/add-new-user'] = 'admin/Admin_con/addedit_user/';
$route['dashboard/edit-user/(:any)'] = 'admin/Admin_con/addedit_user/$1';

// Admin
$route['admin'] = 'admin/Admin_con';
$route['logout'] = 'admin/Admin_con/logout';
$route['translate_uri_dashes'] = TRUE;
$route['404_override'] = 'my404';
