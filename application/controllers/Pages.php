<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pages extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Data_model');
        $this->load->model('admin/Admin_model');
    }

    public function index() {
        $data['slide_data'] = $this->Data_model->get_index_slide_pictures();
        $data['main_page_data'] = $this->Admin_model->mainPageData();
        $this->load->view('index', $data);
    }

    public function about_Us() {
        $this->load->view('about');
    }

    public function contact_us() {
        $this->load->view('contact');
    }

    public function gallery() {
        $table_name ='gallery';
        $base_url = base_url('Pages/gallery');
        $total_rows =$this->Admin_model->get_count($table_name);
        $per_page = 9;
        $uri_segment = 3;
        
        $pagination = set_pagination($base_url, $total_rows, $per_page, $uri_segment);
        $page= $pagination['page'];
        $data['links'] = $pagination['links'];
        $data['gallery_data'] = $this->Admin_model->get_table_data_for_pagination($table_name, $per_page, $page);

       // $this->load->view('pagination', $data);

       // $data['gallery_data'] = $this->Admin_model->get_all_gallery_data();
        $this->load->view('gallery', $data);
    }

    public function services() {
        $this->load->view('services');
    }

}
