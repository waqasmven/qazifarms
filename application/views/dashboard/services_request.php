<?php
$this->load->view('frontend/dashboard/_dash_header');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Ansonika">
        <title><?= $heading ?> - <?= SITETITLE ?></title>

        <!-- Favicons-->
        <link rel="shortcut icon" href="<?= base_url() . DASHTHEME ?>img/favicon.ico" type="image/x-icon">
        <link rel="apple-touch-icon" type="image/x-icon" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-57x57-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?= base_url() . DASHTHEME ?>img/apple-touch-icon-144x144-precomposed.png">

        <!-- Bootstrap core CSS-->
        <link href="<?= base_url() . DASHTHEME ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Main styles -->
        <link href="<?= base_url() . DASHTHEME ?>css/admin.css" rel="stylesheet">
        <!-- Icon fonts-->
        <link href="<?= base_url() . DASHTHEME ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- Plugin styles -->
        <link href="<?= base_url() . DASHTHEME ?>vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
        <!-- Your custom styles -->
        <link href="<?= base_url() . DASHTHEME ?>css/custom.css" rel="stylesheet">
    </head>

    <body class="fixed-nav sticky-footer" id="page-top">
        <?php
        $this->load->view('frontend/dashboard/_dash_nav');
        ?>
        <!-- /Navigation-->
        <div class="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?= base_url('dashboard') ?>">Dashboard</a>
                    </li>
                    <li class="breadcrumb-item active"><?= $heading ?></li>
                </ol>
                <div class="box_general">
                    <div class="header_box">
                        <h2 class="d-inline-block"><?= $heading ?></h2>
                        <div class="filter">
                            <select name="orderby" class="selectbox">
                                <option value="Any status">Any status</option>
                                <option value="Approved">Approved</option>
                                <option value="Pending">Pending</option>
                                <option value="Cancelled">Cancelled</option>
                            </select>
                        </div>
                    </div>
                    <div class="list_general">
                        <ul>
                            <?php foreach($serviceRequests as $service_row){ ?>
                            <li>
                                <!--<figure><img src="img/item_3.jpg" alt=""></figure> -->
                                <h4><a href="<?= base_url('service/'.$service_row['permalink'])?>"><?=$service_row['trade_name']?><i class="approved">Approved</i></a></h4>
                                <ul class="booking_list">
                                    <li><strong>Booking date</strong> <?=$service_row['created_date']?></li>
                                    <li><strong>Booking details</strong><?=$service_row['created_date']?></li>
                                    <li><strong>Address</strong> <?=$service_row['addr_placeholder']?></li>
                                </ul>
                               <!-- <p><a href="#0" class="btn_1 gray"><i class="fa fa-fw fa-envelope"></i> Send Message</a></p>
                                <ul class="buttons">
                                    <li><a href="#0" class="btn_1 gray approve"><i class="fa fa-fw fa-check-circle-o"></i> Approve</a></li>
                                    <li><a href="#0" class="btn_1 gray delete"><i class="fa fa-fw fa-times-circle-o"></i> Cancel</a></li>
                                </ul> -->
                            </li>
                            <?php }?>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid-->
        </div>

        <?php
        $this->load->view('frontend/dashboard/_dash_footer');
        ?>
        <script src="<?= base_url() . SITETHEME ?>assets/js/jquery-2.2.0.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
        <!-- Bootstrap core JavaScript-->
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery/jquery.min.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Page level plugin JavaScript-->
        <script src="<?= base_url() . DASHTHEME ?>vendor/chart.js/Chart.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/datatables/jquery.dataTables.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/datatables/dataTables.bootstrap4.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery.selectbox-0.2.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/retina-replace.min.js"></script>
        <script src="<?= base_url() . DASHTHEME ?>vendor/jquery.magnific-popup.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="<?= base_url() . DASHTHEME ?>js/admin.js"></script>
        <!-- Custom scripts for this page-->
        <script src="<?= base_url() . DASHTHEME ?>js/admin-charts.js"></script>
        <script>
        $(document).ready(function () {
            $("#dash_service_requests").addClass(" active");
        });
    </script>
    </body>
</html>
