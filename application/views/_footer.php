<!-- footer -->
<div class="footer">
    <div class="container">
        <div class="w3agile_footer_grids">
            <div class="col-md-3 agileinfo_footer_grid">
                <div class="agileits_w3layouts_footer_logo">
                    <h2><a href="<?= base_url()?>"><span>Qazi</span>Agriculture<i>Farms</i></a></h2>
                </div>
            </div>
            <div class="col-md-4 agileinfo_footer_grid">
                <h3>Contact Info</h3>
                <h4>Call Us <span><?=SUPPORT_CONTACT?></span></h4>
                <p><?=SUPPORT_ADDRESS?></p>
                <ul class="agileits_social_list">
                    <li><a href="https://www.facebook.com/QaziAgriFarms/" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <!--<li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                    <li><a href="#" class="w3_agile_vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li> -->
                </ul>
            </div>
            <div class="col-md-2 agileinfo_footer_grid agileinfo_footer_grid1">
                <h3>Navigation</h3>
                <ul class="w3layouts_footer_nav">
                    <li><a href="<?= base_url()?>"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Home</a></li>
                    <!--<li><a href="icons.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Web Icons</a></li>
                    <li><a href="typography.html"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Typography</a></li>-->
                    <li><a href="<?= base_url('Contact-us')?>"><i class="fa fa-long-arrow-right" aria-hidden="true"></i>Contact Us</a></li>
                </ul>
            </div>
            <div class="col-md-3 agileinfo_footer_grid">
                <!--<h3>Facebook Posts</h3>-->
                <div class="fb-page" data-href="https://www.facebook.com/QaziAgriFarms/" data-tabs="timeline" data-width="" data-height="300" data-small-header="true" data-adapt-container-width="false" data-hide-cover="true" data-show-facepile="false"><blockquote cite="https://www.facebook.com/QaziAgriFarms/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/QaziAgriFarms/">Qazi Agri Farms</a></blockquote></div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="w3_agileits_footer_copy">
        <div class="container">
            <p>©<?=date('Y')?> <?=SITETITLE?></p>
        </div>
    </div>
</div>
<!-- //footer -->