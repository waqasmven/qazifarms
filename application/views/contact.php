
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('_header') ?>
    </head>

    <body>
        <!-- banner -->
        <div class="banner1">
            <div class="container">
                <?php $this->load->view('_top_nav'); ?>
            </div>
        </div>
        <!-- banner -->
        <!-- bootstrap-pop-up -->
        <div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        Qazi Agri Farms
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <section>
                        <div class="modal-body">
                            <img src="<?= base_url(SITETHEME) ?>images/4.jpg" alt=" " class="img-responsive" />
                            <p><strong>To develop the existing infrastructure of horticulture sector especially in the field of vegetable yield by enabling small farmer to get more out of less resources by using latest trends and technologies in ‘Green House Tunnel Farming’.

                                    The aim and objectives of ‘Qazi Agri Farms’ additionally hope to:-

                                    •Advise and assist the farmers and workers by providing free consultancy services for promotion of ‘Green House Tunnel Farming’

                                    •Provide ample chances in training cadre to jobless (educated & un educated lot) and poor lot of marginalized sectors of our society

                                    •Transfer of technologies to worker class being utilized in different projects.

                                    •To promote own vegetable products globally</strong></p>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <!-- //bootstrap-pop-up -->
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <div class="container">
                <div class="w3layouts_breadcrumbs_left">
                    <ul>
                        <li><i class="fa fa-home" aria-hidden="true"></i><a href="index.html">Home</a><span>/</span></li>
                        <li><i class="fa fa-envelope-o" aria-hidden="true"></i>Contact</li>
                    </ul>
                </div>
                <div class="w3layouts_breadcrumbs_right">
                    <h2>Contact Us</h2>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <!-- //breadcrumbs -->
        <!-- contact -->
        <div class="welcome">
            <div class="container">
                <h3 class="agileits_w3layouts_head">Get in<span> touch</span> with us</h3>
                <div class="w3_agile_image">
                    <img src="<?= base_url(SITETHEME) ?>images/hafi.png" alt=" " class="img-responsive" />
                </div>

                <div class="w3ls_news_grids">
                    <div class="col-md-8 w3_agile_mail_left">
                        <div class="agileits_mail_grid_right1 agile_mail_grid_right1">
                            <form action="#" method="post">
                                <span>
                                    <i>Name</i>
                                    <input type="text" name="Name" placeholder="Enter your name here " required="">
                                </span>
                                <span>
                                    <i>Email</i>
                                    <input type="email" name="Email" placeholder="Enter your email here " required="">
                                </span>
                                <span>
                                    <i>Subject</i>
                                    <input type="text" name="Subject" placeholder="Enter your subject " required="">
                                </span>
                                <span>
                                    <i>Message</i>
                                    <textarea name="Message" placeholder="Type your message here " required=""></textarea>
                                </span>
                                <div class="w3_submit">
                                    <input type="submit" value="Submit Now">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4 w3_agile_mail_right">
                        <div class="w3_agileits_mail_right_grid">
                            <h4>About Plantation</h4>
                            <p>plantation is a large farm which is specialized on farming one type of crop. Plantations grow cash crops, mostly for export, and less for local use.</p>
                            <h5>Follow Us On</h5>
                            <ul class="agileits_social_list">
                                <li><a href="https://www.facebook.com/QaziAgriFarms/" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>

                            </ul>
                            <div class="w3_agileits_mail_right_grid_pos">
                                <img src="<?= base_url(SITETHEME) ?>images/naeem.jpg" alt=" " class="img-responsive" />
                            </div>
                        </div>
                        <div class="w3_agileits_mail_right_grid_main">
                            <div class="w3layouts_mail_grid_left">
                                <div class="w3layouts_mail_grid_left1 hvr-radial-out">
                                    <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                                </div>
                                <div class="w3layouts_mail_grid_left2">
                                    <h3>Mail Us</h3>
                                    <a href="mailto:<?= SUPPORT_EMAIL ?>"><?= SUPPORT_EMAIL ?></a>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="w3layouts_mail_grid_left">
                                <div class="w3layouts_mail_grid_left1 hvr-radial-out">
                                    <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                                </div>
                                <div class="w3layouts_mail_grid_left2">
                                    <h3>Address</h3>
                                    <p><?= SUPPORT_ADDRESS ?></p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="w3layouts_mail_grid_left">
                                <div class="w3layouts_mail_grid_left1 hvr-radial-out">
                                    <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
                                </div>
                                <div class="w3layouts_mail_grid_left2">
                                    <h3>Phone</h3>
                                    <p><?= SUPPORT_CONTACT ?></p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <div id="map"></div>
        <!-- //contact -->
        <script type="text/javascript">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);

            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 11,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(33.6, 49.966667), // New York

                    // How you would like to style the map.
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [{"featureType": "landscape", "stylers": [{"hue": "#FFA800"}, {"saturation": 0}, {"lightness": 0}, {"gamma": 1}]}, {"featureType": "road.highway", "stylers": [{"hue": "#53FF00"}, {"saturation": -73}, {"lightness": 40}, {"gamma": 1}]}, {"featureType": "road.arterial", "stylers": [{"hue": "#FBFF00"}, {"saturation": 0}, {"lightness": 0}, {"gamma": 1}]}, {"featureType": "road.local", "stylers": [{"hue": "#00FFFD"}, {"saturation": 0}, {"lightness": 30}, {"gamma": 1}]}, {"featureType": "water", "stylers": [{"hue": "#00BFFF"}, {"saturation": 6}, {"lightness": 8}, {"gamma": 1}]}, {"featureType": "poi", "stylers": [{"hue": "#679714"}, {"saturation": 33.4}, {"lightness": -25.4}, {"gamma": 1}]}]
                };

                // Get the HTML DOM element that will contain your map
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using our element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);

                // Let's also add a marker while we're at it
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(33.6, 49.966667),
                    map: map,
                    title: 'Snazzy!'
                });
            }
        </script>
        <!-- footer -->
        <?php $this->load->view('_footer') ?>
        <!-- //footer -->
        <!-- menu -->
        <script>
            $(function () {

                initDropDowns($("div.shy-menu"));

            });

            function initDropDowns(allMenus) {

                allMenus.children(".shy-menu-hamburger").on("click", function () {

                    var thisTrigger = jQuery(this),
                            thisMenu = thisTrigger.parent(),
                            thisPanel = thisTrigger.next();

                    if (thisMenu.hasClass("is-open")) {

                        thisMenu.removeClass("is-open");

                    } else {

                        allMenus.removeClass("is-open");
                        thisMenu.addClass("is-open");
                        thisPanel.on("click", function (e) {
                            e.stopPropagation();
                        });
                    }

                    return false;
                });
            }
        </script>
        <!-- //menu -->
        <!-- start-smoth-scrolling -->
        <script type="text/javascript" src="<?= base_url(SITETHEME) ?>js/move-top.js"></script>
        <script type="text/javascript" src="<?= base_url(SITETHEME) ?>js/easing.js"></script>
        <script type="text/javascript">
                    jQuery(document).ready(function ($) {
                        $(".scroll").click(function (event) {
                            event.preventDefault();
                            $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
                        });
                    });
        </script>
        <!-- start-smoth-scrolling -->
        <!-- for bootstrap working -->
        <script src="<?= base_url(SITETHEME) ?>js/bootstrap.js"></script>
        <!-- //for bootstrap working -->
        <!-- here stars scrolling icon -->
        <script type="text/javascript">
            $(document).ready(function () {
                /*
                 var defaults = {
                 containerID: 'toTop', // fading element id
                 containerHoverID: 'toTopHover', // fading element hover id
                 scrollSpeed: 1200,
                 easingType: 'linear'
                 };
                 */

                $().UItoTop({easingType: 'easeOutQuart'});

            });
        </script>
        <!-- //here ends scrolling icon -->
        <script>
            $(document).ready(function () {
                $("#contact").addClass(" active");
            });
        </script>
    </body>
</html>